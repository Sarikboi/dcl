-- Grant connect permission
GRANT CONNECT ON DATABASE dvdrental TO rentaluser;

-- Grant SELECT permission for the "customer" table
GRANT SELECT ON TABLE customer TO rentaluser;

-- Check SELECT permission
SELECT * FROM customer;

-- Create a new user group if it doesn't exist and add the user to the group
DO $$ 
BEGIN
  IF NOT EXISTS (SELECT FROM pg_group WHERE groname = 'rental') THEN
    CREATE GROUP rental;
  END IF;
END $$;

GRANT rental TO rentaluser;

-- Grant INSERT and UPDATE permissions for the "rental" table to the "rental" group
GRANT INSERT, UPDATE ON TABLE rental TO rental;

-- Insert a new row into the "rental" table with a valid staff_id
-- (Replace these values with actual data)
INSERT INTO rental (customer_id, staff_id, inventory_id, rental_date, return_date)
VALUES (1, 2, 456, '2023-11-26', CURRENT_TIMESTAMP);



-- Revoke INSERT permission for the "rental" table from the "rental" group
REVOKE INSERT ON TABLE rental FROM rental;

-- Try to insert new rows into the "rental" table (this should be denied)
-- (Replace these values with actual data)
-- This should result in a permission error
INSERT INTO rental (customer_id, rental_date) VALUES (1, '2023-11-28');

-- Create a personalized role for an existing customer
CREATE ROLE client_Maria_Miller;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO client_Maria_Miller;

-- Configure the role to access only their own data in the "rental" and "payment" tables
GRANT SELECT ON TABLE rental TO client_Maria_Miller;
GRANT SELECT ON TABLE payment TO client_Maria_Miller;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO client_Maria_Miller;

-- Query to check that the user sees only their own data
-- (Replace this value with an actual customer ID)
SET ROLE client_Maria_Miller;
SELECT * FROM rental WHERE customer_id = 7;
SELECT * FROM payment WHERE customer_id = 7;
RESET ROLE;